# Liferea Minimal UI Plugin

Plugin for RSS Reader Liferea to only display headlines for the cleanist possible look.

# Example screenshot (i3 gaps)

![alt text](https://gitlab.com/anonymous133/liferea-minimal-ui-plugin/raw/master/liferea-minimal-plugin.jpg)

# Installation instructions
**Prerequisites**

Since you will be missing UI elements you need to have some keyboard shortcuts in place to work with. Here is how to do it:

## Install Custom Accels plugin

1. In Liferea Open "Tools" > "Plugin Menu"
2. Under "Download Plugin"-tab install the "Custom Accels" Plugin
3. Switch to "Activate Plugins" and Activate "Edit Accels" Plugin
4. Right click on it and go to preferences of "Edit Accels" Plugin
5. Click "Dump Accels"

 Open dumped file and edit the entries like this (Also remove the # infront of it to enable the shortcut)

    ["app.InstallPlugins", ["i"]]
    ["app.launch-selected-item-in-external-browser", ["o"]]
    ["app.mark-all-feeds-read", ["<Primary>r"]]
- Press `i` to display the plugin manager to deactivate the Minimal UI Plugin
- Press `o` to open a selected headline in the external browser
- Press `Control+r` to mark everything as read

6. Select the **Unread** folder in Liferea

## Liferea preferences
This allows to open links with a double click on headlines in your external browser of choice.

1. Go to "Tools" > "Preferences" > "Browser"
2. Uncheck "Open links in Liferea's window."    

## Install the Minimal UI Plugin

1. Place minimal.py and minimal.plugin in ~/.local/share/liferea/plugins
2. In Liferea Open "Tools" > "Plugin Menu"
3. Under "Activate Plugin"-tab activate the "Minimal UI" Plugin

## Deactivate Minimal UI Plugin

Press `i` and disable the plugin

## Limitations / Missing features
- Disable this plugin via a right click context menu item on the item list.
- When marked all as read and a headline was selected that entry will stay in the list. This seems to be an issue in liferea and is filed here: https://github.com/lwindolf/liferea/issues/720
