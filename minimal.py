#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
#
# Liferea Minimal UI Plugin
#
# Copyright (C) 2019 anonymous133 via github
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

# Project url: https://gitlab.com/anonymous133/liferea-minimal-ui-plugin

# used links
# https://stackoverflow.com/questions/89228/calling-an-external-command-in-python
# https://stackoverflow.com/questions/4570859/gtktreeviews-row-right-click 
# https://developer.gnome.org/gtk3/stable/GtkTreeView.html
# https://lzone.de/liferea/blog/Writing+Liferea+Plugins+Tutorial+Part+1
# https://lzone.de/liferea/blog/Writing+Liferea+Plugins+Tutorial+Part+2
# https://lzone.de/liferea/blog/Writing+Liferea+Plugins+Tutorial+Part+3
# https://lzone.de/liferea/blog/Writing+Liferea+Plugins+Tutorial+Part+4
# https://github.com/lwindolf/liferea/blob/master/plugins/plugin-installer.py
# https://github.com/lwindolf/liferea
# https://github.com/lwindolf/liferea/blob/master/plugins/bold-unread.py

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import GObject, Gio, Gtk, Gdk, PeasGtk, Liferea, Pango

def setTextWeightNormal(column, cell, model, iter, data):
    cell.set_property("weight", Pango.Weight.NORMAL)
    return

class MinimalUIPlugin (GObject.Object, Liferea.ShellActivatable):
    __gtype_name__ = "MinimalUIPlugin"

    object = GObject.property (type=GObject.Object)
    shell = GObject.property (type=Liferea.Shell)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate (self): 
        self.setUIVisible(False)
        
        #Set Headline text style to normal because all items are unread items
        self.ticol = self.itemlist.get_column (0)
        area = self.ticol.get_property ("cell-area")
        self.layout = area.get_cells ()
        self.ticol.set_cell_data_func (self.layout[0], setTextWeightNormal)
        self.itemlist.queue_draw ()
        return

    def do_deactivate (self):
        self.setUIVisible(True)

    def setUIVisible(self, bvisible):
        
        #Hide UI elements
        self.shell.lookup("mainwindow").set_show_menubar(bvisible)
        self.shell.lookup("statusbar").set_visible(bvisible)
        self.shell.lookup("scrolledwindow3").set_visible(bvisible)
        self.shell.lookup("normalViewVBox").set_visible(bvisible)
        self.shell.lookup("mainwindow").set_show_menubar(bvisible)
        self.shell.lookup("maintoolbar").set_visible(bvisible)

        normalviewitems = self.shell.lookup ("normalViewItems")
        self.itemlist = normalviewitems.get_child().get_child()
        self.itemlist.set_headers_visible(bvisible)
    
        #Hide/Clear item list columns except Headline
        column = Gtk.TreeView.get_column (self.itemlist, 1)
        Gtk.TreeViewColumn.set_visible (column, bvisible);

        self.ticolfav = self.itemlist.get_column (2)
        self.ticolfav.clear()

        self.ticolattachments = self.itemlist.get_column (3)
        self.ticolattachments.clear()

        self.ticolunread = self.itemlist.get_column (4)
        self.ticolunread.clear()

        self.itemlist.set_show_expanders(bvisible);
        return
